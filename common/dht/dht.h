#ifndef DHT_H
#define DHT_H

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
    char addr[22];
    unsigned int pos;
} serverinfo;

int dht_init(const char *dhtconf,serverinfo **sst,unsigned int *serverNum);
int dht_byhash(serverinfo *sst,unsigned serverNum,serverinfo **svr,unsigned int dhtKeyHash);
int dht_bystring(serverinfo *sst,unsigned serverNum,serverinfo **svr,const char *dhtKey);
int dht_fini(serverinfo *sst);

#ifdef __cplusplus
}
#endif

#endif
