#include <stdio.h>
#include "dht.h"
int main(void)
{
	char *ss = "sst.conf";
	serverinfo *sst,*svr;
	unsigned int serverNum = 0;
	int rv = dht_init(ss,&sst,&serverNum);
	if(rv < 0)
		return -1;
	int i;
	for(i = 0; i < serverNum; ++i)
		printf("ip:%s,pos:%lu\n",sst[i].addr,sst[i].pos);
	//get by uid string
    int count = 0;
    int first = 0,second = 0;
    char key[64]={0};
    for(count = 0; count < 1000000; ++count)
    {
        snprintf(key,sizeof(key),"zhousihai%d",count);
        rv = dht_bystring(sst,serverNum,&svr,key);
        if(rv < 0)
        {
            printf("Get server failed!\n");
        }
        else
        {
            printf("Get svr ip:%s,pos:%lu\n",svr->addr,svr->pos);
            if(svr->pos == 2147483647)
            {
                ++first;
            }
            else
            {
                ++second;
            }
        }
    }
    printf("first:%d,second:%d\n",first,second);
	dht_fini(sst);
	return 0;
}
