#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "dht.h"

/** \brief Retrieve a serverinfo struct for one a sever definition.
  * \param line The entire server definition in plain-text.
  * \return A serverinfo struct, parsed from the given definition. */
static serverinfo
read_server_line( char* line )
{
    const char* delim = ",";
    serverinfo server;
    server.pos = 0;

    char* tok = strtok( line, delim );
    if ( ( strlen( tok ) - 1 ) < 23 )
    {
        char* mem = 0;
        char* endptr = 0;

        strncpy( server.addr, tok, strlen( tok ) );
        server.addr[ strlen( tok ) ] = '\0';

        tok = strtok( 0, delim );
        /* We do not check for a NULL return earlier because strtok will
         * always return at least the first token; hence never return NULL.
         */
        if ( tok == 0 )
        {
            printf("Unable to find delimiter!\n" );
            server.pos = 0;
        }
        else
        {
            mem = (char *)malloc( strlen( tok ) );
            strncpy( mem, tok, strlen( tok ) - 1 );
            mem[ strlen( tok ) - 1 ] = '\0';

            errno = 0;
            server.pos = strtol( mem, &endptr, 10 );
            if ( errno == ERANGE || endptr == mem )
            {
                printf("Invalid pos value!\n");
                server.pos = 0;
            }

            free( mem );
        }
    }

    return server;
}


/** \brief Retrieve all server definitions from a file.
  * \param filename The full path to the file which contains the server definitions.
  * \param count The value of this pointer will be set to the amount of servers which could be parsed.
  * \param memory The value of this pointer will be set to the total amount of allocated memory across all servers.
  * \return A serverinfo array, containing all servers that could be parsed from the given file. */
static serverinfo*
read_server_definitions( const char* filename, unsigned int* count, unsigned int* memory )
{
    serverinfo* slist = 0;
    unsigned int lineno = 0;
    unsigned int numservers = 0;
    unsigned int memtotal = 0;

    FILE* fi = fopen( filename, "r" );
    while ( fi && !feof( fi ) )
    {
        char sline[128] = "";

        fgets( sline, 127, fi );
        lineno++;

        if ( strlen( sline ) <= 2 || sline[0] == '#' )
            continue;

        serverinfo server = read_server_line( sline );
        if ( server.pos > 0 && strlen( server.addr ) )
        {
            slist = (serverinfo*)realloc( slist, sizeof( serverinfo ) * ( numservers + 1 ) );
            memcpy( &slist[numservers], &server, sizeof( serverinfo ) );
            numservers++;
            memtotal += server.pos;
        }
        else
        {
            /* This kind of tells the parent code that
             * "there were servers but not really"
             */
            *count = 1;
            free( slist );
            printf("error:(line %d in %s)\n",
                lineno, filename );
            return 0;
        }
    }

    if ( !fi )
    {
        printf("File %s doesn't exist!\n", filename );

        *count = 0;
        return 0;
    }
    else
    {
        fclose( fi );
    }

    *count = numservers;    
    *memory = memtotal;
    return slist;
}

inline
unsigned int logicalShiftToLeft(unsigned int val, unsigned int shiftBits)
{
	shiftBits &= 0x1f;
	val = (val>>(0x20 - shiftBits)) | (val << shiftBits);
	return val;
}

/*string hash*/
inline
unsigned int stringHash(const char * str)
{
	unsigned int hash = 0;
	while( *str != '\0' )
	{
		hash += logicalShiftToLeft(hash,19) + *str;
		++str;	
	}
	
	return hash;
}

inline 
unsigned int intHash(unsigned int h)
{
    // Spread bits to regularize both segment and index locations,
    // using variant of single-word Wang/Jenkins hash.
     h += (h <<  15) ^ 0xffffcd7d;
     h ^= (h >> 10);
     h += (h <<   3);
     h ^= (h >>  6);
     h += (h <<   2) + (h << 14);
      
    return h ^ (h >> 16);

}

/* return the electoral server's index by hash ring arithmetic.
*  Using binary searching arithmetic.
*/
inline
unsigned ringArithmetic(unsigned int * sortedServerPosArray,unsigned serverNum,unsigned int dhtKeyHash)
{
	unsigned upperBound = serverNum - 1;
	unsigned low = 0, high = upperBound, middle;
	unsigned result = low;
	
	if( serverNum == 0 )
		return (unsigned)(-1);
	
	while( low <= high )
	{
		middle = (low + high)/2;
		if ( sortedServerPosArray[middle] < dhtKeyHash )
		{
			if( middle >= upperBound ) return 0;
			low = middle + 1;
			result = low;
		}
		else
		{ 
			if( middle == 0 ) return 0;
			high = middle - 1;
		}
	}
	
	return result;
}

inline
unsigned ringArithmetic2(unsigned int * sortedServerPosArray,unsigned serverNum,const char * dhtKey)
{
	unsigned int keyHash = stringHash(dhtKey);
	return ringArithmetic(sortedServerPosArray, serverNum, keyHash);
}

int dht_init(const char *dhtconf,serverinfo **sst,unsigned int *serverNum)
{
	serverinfo *svrinfo = NULL;
	unsigned int mem = 0;
	svrinfo = read_server_definitions(dhtconf,serverNum,&mem);
	if(!svrinfo)
		return -1;
	*sst = svrinfo;
	return 0;
}

int dht_byhash(serverinfo *sst,unsigned serverNum,serverinfo **svr,unsigned int dhtKeyHash)
{
	if(serverNum <= 0)
		return -1;
	unsigned int sortedServerPosArray[serverNum];
	unsigned i;
	for( i = 0; i < serverNum; ++i)
	{
		sortedServerPosArray[i] = sst[i].pos;
	}
        unsigned idx = ringArithmetic(sortedServerPosArray,serverNum,dhtKeyHash);
	*svr = &sst[idx];
	return 0;
}

int dht_bystring(serverinfo *sst,unsigned serverNum,serverinfo **svr,const char *dhtKey)
{
	if(serverNum <= 0)
		return -1;
	unsigned int keyHash = stringHash(dhtKey);
    keyHash = intHash(keyHash);
    printf("keyHash:%u\n",keyHash);
	unsigned int sortedServerPosArray[serverNum];
	unsigned int i;
	for( i = 0; i < serverNum; ++i)
	{
		sortedServerPosArray[i] = sst[i].pos;
	}
    unsigned idx = ringArithmetic(sortedServerPosArray,serverNum,keyHash);
	*svr = &sst[idx];
	return 0;
}

int dht_fini(serverinfo *sst)
{
	if(sst)
		free(sst);
	return 0;
}

